
Other scripts
=============

This folder contains the following scripts:

- scripts_to_generate_coordinates.tar.xz - scripts to generate the gene co-ordinates
- repair_analysis_FigS7.tar.xz - scripts to perform the NER repair analysis shown in Supp. Fig. 7

Download the scripts using the below commands:

.. code::

   wget https://bitbucket.org/bbglab/intron_exon_mutrate/downloads/scripts_to_generate_coordinates.tar.xz
   tar -xJf scripts_to_generate_coordinates.tar.xz
   wget https://bitbucket.org/bbglab/intron_exon_mutrate/downloads/repair_analysis_FigS7.tar.xz
   tar -xJf repair_analysis_FigS7.tar.xz

.. warning::

   Execute these commands from this folder so that the files are in the right directory.
