
Results
=======

The results folder is where the notebooks write their output to.

Some notebooks cannot be run without some data that we are not providing with this code.
In particular, the `signature_probability <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/signature_probability.ipynb>`_ 
and the `gene_and_sample_analysis <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/gene_and_sample_analysis.ipynb>`_ notebooks.

Thus, we have uploaded the results of these notebooks
to `our repository <https://bitbucket.org/bbglab/intron_exon_mutrate/downloads/>`_

You can either download those files by yourself or execute:

.. code::

   wget https://bitbucket.org/bbglab/intron_exon_mutrate/downloads/signature_probability_results.tar.xz
   tar -xJf signature_probability_results.tar.xz
   wget https://bitbucket.org/bbglab/intron_exon_mutrate/downloads/gene_and_sample_analysis_results.tar.xz
   tar -xJf gene_and_sample_analysis_results.tar.xz
   wget https://bitbucket.org/bbglab/intron_exon_mutrate/downloads/supplementary_data.tar.xz
   tar -xJf supplementary_data.tar.xz

.. warning::

   Execute these commands from this folder so that the files are in the right directory.
