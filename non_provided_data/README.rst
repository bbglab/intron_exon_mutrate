Non provided data
=================

We used this folder to contain all the data used in our analysis
that is protected or easily downloadable and therefore is not shared by us.

The sources of these data are can be found the paper.



replication_timing.txt
   File with information about the replication time.
   The information was obtained from these papers:
   
   - Genetic Variation in Human DNA Replication Timing
   - Differential Relationship of DNA Replication Timing to Different Forms of Human Mutation and Variation

   And downloaded from: `<http://mccarrolllab.com/resources/>`_



expression.tsv
    Tabulated file that contains expression information of each gene in different samples.

   TODO check
   Expression profiles for all 505 tumors. Includes ~30.000 GENCODE protein-coding genes and lncRNAs, quantified based on available .bam files as decribed in Akrami et al 2013 (PMID: 24265805). 




mutations
---------

   Folder with the mutational data files.

   For each tumors types, one tab separated file (without header) and BED format:

   ==========  ==============  ============  =========  ==========  =========  ====
   Chromosome  Start position  End position  Reference  Alteration  Sample ID  Type
   chr1	       19267           19268         C          T           XXX        subs
   ==========  ==============  ============  =========  ==========  =========  ====

   The file should be named as the cancer type (e.g. brca.bed.gz)

   The mutational data uses HG19 as reference genome.


histonmarks
-----------

   Folder with downloaded peak and read coordinates and genome-wide coverage of 29 chromatin features across 127 cell lines and primary cell types from the Epigenomics Roadmaps.


