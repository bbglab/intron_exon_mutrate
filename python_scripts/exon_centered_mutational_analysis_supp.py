import math
import pybedtools
import pandas as pd
import numpy as np
from bgreference import hg19

from .analysis_functions import remove_version_ensembl



nucleotides = set(['A', 'T', 'C', 'G'])

######Exons analysis

def obs_exp_muts_rand(mutations_file, clusters_file, exon_coords_file, signatures_file, tumor_type, cluster_id, middle_distance_threshold):
    """
    Get positions of the exons that correspond to the central position and a number of base pair
    equal to middle_distance_threshold on each side
    Compute the number of mutations per position under analysis.To computed the expected mutation, for
    each exon-centered region (with at least one observed mutation), we randomize the mutation(using the
    values of the signatures_file)for 1000 times and compute the average mutation observed in each position.
    
    Args:
        mutations_file: path
        clusters_file: path
        exon_coords_file: path
        signatures_file: path
        tumor_type (str):
        cluster_id (str):
        middle_distance_threshold (int): amount of position to use on around the center (on each direction)

    Returns:
        :class:`~pandas.DataFrame`. Table with the relative position (to the exon center)
        the number of mutations observed and expected in that position.

    """

    # Load files
    ## Mutations
    mutations_df = pd.read_csv(mutations_file, sep="\t", header=None)
    mutations_df.columns = ['CHROMOSOME', 'START', 'POSITION', 'REF', 'ALT', 'SAMPLE', 'TYPE']
    mutations_df = mutations_df[mutations_df['TYPE'] == 'subs']

    ## Clusters
    clusters_df = pd.read_csv(clusters_file, sep='\t', low_memory=False)
    ttype_clusters_df = clusters_df[clusters_df['ctype'] == tumor_type]
    cluster_samples = ttype_clusters_df[ttype_clusters_df['cluster'] == cluster_id]['sample_id'].tolist()

    ## Exon coordinates
    exons_coords_df = pd.read_csv(exon_coords_file, sep="\t", header=None, low_memory=False)
    exons_coords_df.columns = ['chr', 'start', 'end', 'ensembl', 'symbol', 'strand']
    exons_coords_df = exons_coords_df[['chr', 'start', 'end', 'ensembl']]
    exons_coords_df['ensembl'] = exons_coords_df.apply(lambda x: remove_version_ensembl(x, 'ensembl'), axis=1)
    exons_coords_df.columns = ['chr', 'start', 'end', 'ensembl']
    exons_coords_symbol_df = exons_coords_df[['chr', 'start', 'end', 'ensembl']]

    # Signatures
    all_signatures = pd.read_csv(signatures_file, sep='\t')
    probability_name = 'Probability_' + tumor_type + '_' + cluster_id
    sub_signatures_df = all_signatures[['mutation', probability_name]]
    signatures_dict = sub_signatures_df.set_index('mutation').T.to_dict()

    # Keep only mutation in samples of the cluster under analysis
    cluster_mutations_df = mutations_df[mutations_df['SAMPLE'].isin(cluster_samples)]
    sub_mutations_df = cluster_mutations_df[['CHROMOSOME', 'POSITION', 'SAMPLE']].copy()
    sub_mutations_df['START'] = sub_mutations_df['POSITION'] - 1
    sub_mutations_df = sub_mutations_df[['CHROMOSOME', 'START', 'POSITION', 'SAMPLE']]
    sub_mutations_df.columns = ['chr', 'start', 'end', 'sample']

    # Get the positions that correspond to the center of the exons and the positions around
    # (within a distance indicated by middle_distance_threshold)
    sub_mutations_bed = pybedtools.BedTool.from_dataframe(sub_mutations_df)

    exons_coords_symbol_df['exon_size'] = (exons_coords_symbol_df['end'] - exons_coords_symbol_df['start'])
    exons_coords_symbol_df['exon_middle_start'] = (exons_coords_symbol_df['start'] + exons_coords_symbol_df['exon_size']/2)
    exons_coords_symbol_df['exon_middle_start'] = exons_coords_symbol_df.apply(lambda x:
                                                                       math.floor(x['exon_middle_start']),
                                                                       axis=1)

    exons_coords_symbol_df['exon_middle_end'] = exons_coords_symbol_df['exon_middle_start'] + 1
    exons_coords_symbol_df['region_start'] = exons_coords_symbol_df['exon_middle_start'] - middle_distance_threshold
    exons_coords_symbol_df['region_end'] = exons_coords_symbol_df['exon_middle_end'] + middle_distance_threshold
    sub_exons_coords = exons_coords_symbol_df[['chr', 'region_start', 'region_end', 'ensembl',
                                          'exon_size', 'exon_middle_start',
                                          'exon_middle_end']]
    sub_exons_coords_bed = pybedtools.BedTool.from_dataframe(sub_exons_coords)

    # Filter mutations by the position of interest
    my_bed = sub_exons_coords_bed.intersect(sub_mutations_bed, wao=True)

    mutations_in_range = pd.read_table(my_bed.fn, names = ['range_chr', 'range_start', 'range_end',
                            'ensembl','region_size', 'region_middle_start', 'region_middle_end',
                            'mut_chr', 'mut_start', 'mut_end', 'sample', 'overlap_bp'],  sep="\s+", index_col=False)

    mutations_in_range = mutations_in_range[mutations_in_range['overlap_bp'] != 0]
    mutations_in_range['relative_start'] = mutations_in_range['mut_start'] - mutations_in_range['region_middle_start']
    exon_counts = mutations_in_range['relative_start'].value_counts().to_dict()

    # Count mutations per position
    for i in range(-middle_distance_threshold,middle_distance_threshold):
        if i not in exon_counts.keys():
            exon_counts[i] = 0

    my_exon_results_lol = list()

    for my_position in exon_counts.keys():
        my_count = exon_counts[my_position]

        my_exon_results_lol.append([my_position, my_count])

    my_exon_results_df = pd.DataFrame(my_exon_results_lol)
    my_exon_results_df.columns = ['position', 'muts_count']
    my_exon_results_df = my_exon_results_df.sort_values(by='position')
    
    # compute expected mutation by randomization approach
    # initialize cont for radomized mutations per position
    RANDOMIZATION = 1000
    exon_rand_muts = {}
    for i in range(-middle_distance_threshold, middle_distance_threshold+1):
        exon_rand_muts[i] = 0

    # select exonic regions with more than one mutations
    for df, mutcnt in mutations_in_range.groupby(['range_chr', 'range_start', 'range_end', 'ensembl', 'region_size',
           'region_middle_start', 'region_middle_end']).size().iteritems():

        position_prob_lol = list()
        relative_pos_lol = list()

        my_chr = df[0][3:]
        my_start = int(df[1])
        my_end = int(df[2])
        n_bases = my_end - my_start

        my_exon_bases = hg19(my_chr, my_start, size=n_bases+2)

        my_trinucleotides = [my_exon_bases[i:i+3] for i in range(len(my_exon_bases)-2)]

        i = 0
        for my_trinucleotide in my_trinucleotides:

            my_ref_base = my_trinucleotide[1].upper()
            my_alt_bases = nucleotides - set(my_ref_base)
            my_base_probs = 0
            previous_base = my_trinucleotide[0]
            next_base = my_trinucleotide[2]

            for alt_base in my_alt_bases:
                tri_ref = previous_base + str(my_ref_base) + next_base
                tri_alt = previous_base + str(alt_base) + next_base
                my_key = str((tri_ref, tri_alt))

                try:
                    my_prob = signatures_dict[my_key]['Probability_' + tumor_type + '_' + cluster_id]
                    my_base_probs = my_base_probs + my_prob
                except:
                    None

            normalized_i = i - middle_distance_threshold
            position_prob_lol.append(my_base_probs)
            relative_pos_lol.append(normalized_i)
            i += 1

        # normalize the probability vector and perform sampling
        prb_vector = np.array(position_prob_lol)
        prb_vector = prb_vector / prb_vector.sum() 
        pos_vector = np.array(relative_pos_lol)
        mutation_rand_pos = np.random.choice(pos_vector, size=mutcnt*RANDOMIZATION, replace=True, p=prb_vector)
        
        for pos in mutation_rand_pos:
            exon_rand_muts[pos]+=1

    df_rand = pd.DataFrame.from_dict(exon_rand_muts, orient='index')
    df_rand['position'] = df_rand.index
    df_rand['expected_muts'] = df_rand[0]/RANDOMIZATION
    final_position_prob_df = df_rand[['position', 'expected_muts']]

    return pd.merge(my_exon_results_df, final_position_prob_df, on='position')