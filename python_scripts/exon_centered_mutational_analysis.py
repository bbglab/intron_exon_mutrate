import math
import pybedtools
import pandas as pd
from bgreference import hg19

from .analysis_functions import remove_version_ensembl



nucleotides = set(['A', 'T', 'C', 'G'])

######Exons analysis

def obs_exp_muts(mutations_file, clusters_file, exon_coords_file, signatures_file, tumor_type, cluster_id, middle_distance_threshold):
    """
    Get positions of the exons that correspond to the central position and a number of base pair
    equal to middle_distance_threshold on each side
    Compute the number of mutations per position under analysis.
    For each of the relative positions, compute the sum of the probabilities of each possible alteration
    taking into account its context (using the values of the signatures_file) wheighted by the
    number of observed mutations.


    Args:
        mutations_file: path
        clusters_file: path
        exon_coords_file: path
        signatures_file: path
        tumor_type (str):
        cluster_id (str):
        middle_distance_threshold (int): amount of position to use on around the center (on each direction)

    Returns:
        :class:`~pandas.DataFrame`. Table with the relative position (to the exon center)
        the number of mutations observed and expected in that position.

    """

    # Load files
    ## Mutations
    mutations_df = pd.read_csv(mutations_file, sep="\t", header=None)
    mutations_df.columns = ['CHROMOSOME', 'START', 'POSITION', 'REF', 'ALT', 'SAMPLE', 'TYPE']
    mutations_df = mutations_df[mutations_df['TYPE'] == 'subs']

    ## Clusters
    clusters_df = pd.read_csv(clusters_file, sep='\t', low_memory=False)
    ttype_clusters_df = clusters_df[clusters_df['ctype'] == tumor_type]
    cluster_samples = ttype_clusters_df[ttype_clusters_df['cluster'] == cluster_id]['sample_id'].tolist()

    ## Exon coordinates
    exons_coords_df = pd.read_csv(exon_coords_file, sep="\t", header=None, low_memory=False)
    exons_coords_df.columns = ['chr', 'start', 'end', 'ensembl', 'symbol', 'strand']
    exons_coords_df = exons_coords_df[['chr', 'start', 'end', 'ensembl']]
    exons_coords_df['ensembl'] = exons_coords_df.apply(lambda x: remove_version_ensembl(x, 'ensembl'), axis=1)
    exons_coords_df.columns = ['chr', 'start', 'end', 'ensembl']
    exons_coords_symbol_df = exons_coords_df[['chr', 'start', 'end', 'ensembl']]

    # Signatures
    all_signatures = pd.read_csv(signatures_file, sep='\t')
    probability_name = 'Probability_' + tumor_type + '_' + cluster_id
    sub_signatures_df = all_signatures[['mutation', probability_name]]
    signatures_dict = sub_signatures_df.set_index('mutation').T.to_dict()

    # Keep only mutation in samples of the cluster under analysis
    cluster_mutations_df = mutations_df[mutations_df['SAMPLE'].isin(cluster_samples)]
    sub_mutations_df = cluster_mutations_df[['CHROMOSOME', 'POSITION', 'SAMPLE']].copy()
    sub_mutations_df['START'] = sub_mutations_df['POSITION'] - 1
    sub_mutations_df = sub_mutations_df[['CHROMOSOME', 'START', 'POSITION', 'SAMPLE']]
    sub_mutations_df.columns = ['chr', 'start', 'end', 'sample']

    # Get the positions that correspond to the center of the exons and the positions around
    # (within a distance indicated by middle_distance_threshold)
    sub_mutations_bed = pybedtools.BedTool.from_dataframe(sub_mutations_df)

    exons_coords_symbol_df['exon_size'] = (exons_coords_symbol_df['end'] - exons_coords_symbol_df['start'])
    exons_coords_symbol_df['exon_middle_start'] = (exons_coords_symbol_df['start'] + exons_coords_symbol_df['exon_size']/2)
    exons_coords_symbol_df['exon_middle_start'] = exons_coords_symbol_df.apply(lambda x:
                                                                       math.floor(x['exon_middle_start']),
                                                                       axis=1)

    exons_coords_symbol_df['exon_middle_end'] = exons_coords_symbol_df['exon_middle_start'] + 1
    exons_coords_symbol_df['region_start'] = exons_coords_symbol_df['exon_middle_start'] - middle_distance_threshold
    exons_coords_symbol_df['region_end'] = exons_coords_symbol_df['exon_middle_end'] + middle_distance_threshold
    sub_exons_coords = exons_coords_symbol_df[['chr', 'region_start', 'region_end', 'ensembl',
                                          'exon_size', 'exon_middle_start',
                                          'exon_middle_end']]
    sub_exons_coords_bed = pybedtools.BedTool.from_dataframe(sub_exons_coords)

    # Filter mutations by the position of interest
    my_bed = sub_exons_coords_bed.intersect(sub_mutations_bed, wao=True)

    mutations_in_range = pd.read_table(my_bed.fn, names = ['range_chr', 'range_start', 'range_end',
                            'ensembl','region_size', 'region_middle_start', 'region_middle_end',
                            'mut_chr', 'mut_start', 'mut_end', 'sample', 'overlap_bp'],  sep="\s+", index_col=False)

    mutations_in_range = mutations_in_range[mutations_in_range['overlap_bp'] != 0]
    mutations_in_range['relative_start'] = mutations_in_range['mut_start'] - mutations_in_range['region_middle_start']
    exon_counts = mutations_in_range['relative_start'].value_counts().to_dict()

    # Count mutations per position
    for i in range(-middle_distance_threshold,middle_distance_threshold):
        if i not in exon_counts.keys():
            exon_counts[i] = 0

    my_exon_results_lol = list()

    for my_position in exon_counts.keys():
        my_count = exon_counts[my_position]

        my_exon_results_lol.append([my_position, my_count])

    my_exon_results_df = pd.DataFrame(my_exon_results_lol)
    my_exon_results_df.columns = ['position', 'muts_count']
    my_exon_results_df = my_exon_results_df.sort_values(by='position')

    # For each exonic region under analysis
    # Get its relative position to the center of the exone
    # Add the probability that is the sum of the probabilities of
    final_position_prob_df = pd.DataFrame()
    for my_row in sub_exons_coords.values.tolist():
        position_prob_lol = list()

        my_chr = my_row[0][3:]
        my_start = int(my_row[1])
        my_end = int(my_row[2])
        n_bases = my_end - my_start

        my_region_muts = len(mutations_in_range[(mutations_in_range['range_start'] >= my_start) &
                               (mutations_in_range['range_end'] <= my_end) &
                               (mutations_in_range['range_chr'] == 'chr'+my_chr)])

        if my_region_muts == 0:
            continue

        my_exon_bases = hg19(my_chr, my_start, size=n_bases+2)

        my_trinucleotides = [my_exon_bases[i:i+3] for i in range(len(my_exon_bases)-2)]

        i = 0
        for my_trinucleotide in my_trinucleotides:

            my_ref_base = my_trinucleotide[1].upper()
            my_alt_bases = nucleotides - set(my_ref_base)
            my_base_probs = 0
            previous_base = my_trinucleotide[0]
            next_base = my_trinucleotide[2]

            for alt_base in my_alt_bases:
                tri_ref = previous_base + str(my_ref_base) + next_base
                tri_alt = previous_base + str(alt_base) + next_base
                my_key = str((tri_ref, tri_alt))

                try:
                    my_prob = signatures_dict[my_key]['Probability_' + tumor_type + '_' + cluster_id]
                    my_base_probs = my_base_probs + my_prob
                except:
                    None

            normalized_i = i - middle_distance_threshold
            my_row = [normalized_i, my_base_probs]
            position_prob_lol.append(my_row)
            i += 1

        position_prob_df = pd.DataFrame(position_prob_lol)
        position_prob_df.columns = ['position', 'probability']

        my_total = sum(position_prob_df['probability'])
        position_prob_df['probability'] = position_prob_df['probability']/my_total
        position_prob_df['expected_muts'] = position_prob_df['probability']*my_region_muts

        position_prob_df = position_prob_df[['position', 'expected_muts']]

        # Sum the expected results
        if len(final_position_prob_df) == 0:
            final_position_prob_df = position_prob_df
        else:
            final_position_prob_df['expected_muts'] = (final_position_prob_df['expected_muts'] +
                                                       position_prob_df['expected_muts'])

    return pd.merge(my_exon_results_df, final_position_prob_df, on='position')
