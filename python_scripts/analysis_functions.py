import pybedtools
import numpy as np
import pandas as pd
from collections import Counter
from bgreference import hg19

def remove_version_ensembl(x, colname):
    """
    Remove version from the ensembl ID

    Args:
        x: dataframe row
        colname: column name

    Returns:
        str. Text before a '.'

    """
    ensembl_id = x[colname]
    ensembl_id_only = ensembl_id.split('.')[0]

    return ensembl_id_only


def synonymous_or_not(x):
    """
    Classifies consequence types between synonymous and non synonymous
    using the RANK column

    Args:
        x: dataframe row

    Returns:
        str. *synonymous* or *non_synonymous*

    """

    my_rank = x['RANK']

    if my_rank < 15:
        my_consequence = 'non_synonymous'

    else:
        my_consequence = 'synonymous'

    return my_consequence


def parse_rep_time_data(replication_time_path, whole_gene_coords):
    """
    Compute the mean replication time of each gene (using the replication time of each of regions)

    Args:
        replication_time_path (str): path to a tabulated file with 3 columns (chomosome position and replication time)
        whole_gene_coords (:class:`~pandas.DataFrame`): table with a genomic coordinates table

    Returns:
        :class:`~pandas.DataFrame`.

    """
    replication_time_df = pd.read_csv(replication_time_path, sep='\t', header=None)
    replication_time_df[0] = 'chr' + replication_time_df[0].astype(str)

    replication_time_df.columns = ['chr', 'position', 'rep_time']
    replication_time_df['position'] = replication_time_df['position'].astype(np.int64)

    replication_time_df['start'] = replication_time_df['position'] - 1
    replication_time_df_tointersect = replication_time_df[['chr', 'start', 'position']]

    replication_time_bed = pybedtools.BedTool.from_dataframe(replication_time_df_tointersect)
    whole_gene_coords_bed = pybedtools.BedTool.from_dataframe(whole_gene_coords)

    rep_time_gene_bed = whole_gene_coords_bed.intersect(replication_time_bed)
    rep_time_gene_df = pd.read_table(rep_time_gene_bed.fn, names=['chr', 'start', 'position', 'gene'])
    rep_time_gene_df = rep_time_gene_df[['chr', 'position', 'gene']]

    final_gene_rep_time_df = pd.merge(rep_time_gene_df, replication_time_df, on=['chr', 'position'])
    final_gene_rep_time_df = final_gene_rep_time_df[['chr', 'position', 'gene', 'rep_time']]
    sub_final_gene_rep_time_df = final_gene_rep_time_df[['gene', 'rep_time']]

    rep_time_mean_per_gene_df = sub_final_gene_rep_time_df.groupby('gene').mean()
    rep_time_mean_per_gene_df = rep_time_mean_per_gene_df.sort_values(by='rep_time', ascending=False)
    rep_time_mean_per_gene_df = rep_time_mean_per_gene_df[np.isfinite(rep_time_mean_per_gene_df['rep_time'])]

    whole_gene_coords_bed.delete_temporary_history(ask=False)
    replication_time_bed.delete_temporary_history(ask=False)
    rep_time_gene_bed.delete_temporary_history(ask=False)

    return rep_time_mean_per_gene_df


def precompute_counts(my_genes_list, exons_coords_symbol_df, introns_coords_symbol_df, nucleotides):
    """
    For each gene, count all trinucleotides seen. Set a counter where the key is the
    combination of the reference triplet and each of the possible alterations
    (taken from the remaining nuclotides).
    As the count is only done for the reference, each combination of reference triplet
    and possible alteration has the same count as the reference triplet.

    .. note::

       The first and last nucleotide of each exon or intron are do not contribute to the counts.


    Args:
        my_genes_list (list): list of gene Ensembl identifiers
        exons_coords_symbol_df (:class:`~pandas.DataFrame`): table with the genomic coordinates of the exons
        introns_coords_symbol_df (:class:`~pandas.DataFrame`): table with the genomic coordinates of the introns
        nucleotides (set): nucleotides to consider (typically A, C, G and T)

    Returns:
        dict. For each gene, the counts on exons and introns are retrieved.

    """

    counters_dict = dict()

    for my_gene in my_genes_list:

        introns_counter = Counter()
        exons_counter = Counter()

        # Exons
        try:
            exons_gene = exons_coords_symbol_df[exons_coords_symbol_df['ensembl'] == my_gene]
            exons_gene_lol = exons_gene.values.tolist()

            for my_exon in exons_gene_lol:

                my_chr = my_exon[0][3:]
                my_start = int(my_exon[1])
                my_end = int(my_exon[2])

                n_bases = my_end - my_start

                my_exon_bases = hg19(my_chr, my_start+1, n_bases)

                for i in range(len(my_exon_bases)):

                    my_relative_position = i + my_start

                    signature = hg19(my_chr, my_relative_position, 3)

                    my_ref_base = signature[1].upper()

                    my_alt_bases = nucleotides - set(my_ref_base)

                    for alt_base in my_alt_bases:

                        tri_ref = signature[0] + str(my_ref_base) + signature[-1]
                        tri_alt = signature[0] + str(alt_base) + signature[-1]

                        my_key = tuple([tri_ref, tri_alt])

                        exons_counter[my_key] += 1

        except ValueError:
            print("problem with exons from gene: ", my_gene)

        # Introns
        try:
            introns_gene = introns_coords_symbol_df[introns_coords_symbol_df['ensembl'] == my_gene]
            introns_gene_lol = introns_gene.values.tolist()

            for my_intron in introns_gene_lol:

                my_chr = my_intron[0][3:]
                my_start = int(my_intron[1])
                my_end = int(my_intron[2])

                n_bases = my_end - my_start

                my_intron_bases = hg19(my_chr, my_start+1, n_bases)

                for i in range(len(my_intron_bases)):

                    my_relative_position = i + my_start

                    signature = hg19(my_chr, my_relative_position, 3)

                    my_ref_base = signature[1].upper()

                    my_alt_bases = nucleotides - set(my_ref_base)

                    for alt_base in my_alt_bases:

                        tri_ref = signature[0] + str(my_ref_base) + signature[-1]
                        tri_alt = signature[0] + str(alt_base) + signature[-1]

                        my_key = tuple([tri_ref, tri_alt])

                        introns_counter[my_key] += 1

        except ValueError:
            print("problem with introns from gene: ", my_gene)

        counters_dict[(my_gene, 'exons_count')] = exons_counter
        counters_dict[(my_gene, 'introns_count')] = introns_counter

    return counters_dict



def build_my_positions_result2(genes_list, exons_coords_symbol_df, tb_consequence_type, CONSEQUENCE_RANK):
    """
    For a list of genes, get the consequence type associated with each combination of reference and
    alternate triplets.

    Args:
        genes_list (list): list of genes Ensembl identifiers
        exons_coords_symbol_df (:class:`~pandas.DataFrame`): table with the exons genomic coordinates
        tb_consequence_type: tabix with the consequence types
        CONSEQUENCE_RANK (:class:`~pandas.DataFrame`): table that maps each consequence type with its type
        (synonimous or non-synonimous)

    Returns:
        :class:`~pandas.DataFrame`. Table with the following fiels:
        chromosome, position, ref, alt, ensembl ID, consequence type, type of the consequence,
        reference triplet, alternate triplet

    """
    my_position_results = list()

    for my_gene in genes_list:

        try:
            exons_gene = exons_coords_symbol_df[exons_coords_symbol_df['ensembl'] == my_gene]
            exons_gene_lol = exons_gene.values.tolist()

            for my_exon in exons_gene_lol:

                my_chr = my_exon[0][3:]
                my_start = int(my_exon[1])
                my_end = int(my_exon[2])

                res_exons = tb_consequence_type.querys("{}:{}-{}".format(my_chr, my_start, my_end))

                for my_result in res_exons:
                    if len(my_result) < 6:
                        continue

                    my_result[5] = my_result[5].rstrip('\r')

                    signature = hg19(my_result[0], int(my_result[1])-1, 3)

                    tri_ref = signature[0] + my_result[2] + signature[-1]
                    tri_alt = signature[0] + my_result[3] + signature[-1]

                    my_result.append(tri_ref)
                    my_result.append(tri_alt)
                    my_result.append(my_gene)

                    my_position_results.append(my_result)

        except ValueError:
            print("problem with gene: ", my_gene)

    my_position_results_df = pd.DataFrame(my_position_results)
    my_position_results_df.columns = ['CHROMOSOME', 'POSITION', 'REF', 'ALT', 'ENSEMBL_NOVER', 'CONSEQUENCE', 'TRI_REF',
                                      'TRI_ALT', 'ENSEMBL']

    my_position_results_df = my_position_results_df.drop('ENSEMBL_NOVER', 1)
    my_position_results_df = my_position_results_df[
        ['CHROMOSOME', 'POSITION', 'REF', 'ALT', 'ENSEMBL', 'CONSEQUENCE', 'TRI_REF',
         'TRI_ALT']]

    my_position_results_df = pd.merge(my_position_results_df, CONSEQUENCE_RANK, on='CONSEQUENCE')
    my_position_results_df['POSITION'] = my_position_results_df['POSITION'].astype(int)
    my_position_results_df['CHROMOSOME'] = my_position_results_df['CHROMOSOME'].astype(str)

    return my_position_results_df



def get_muts_per_gene(mutations_df, exons_coords_df, introns_coords_df):
    """
    Intersects the mutations and the coordinates file getting mutations in introns and exons

    Args:
        mutations_df (:class:`~pandas.DataFrame`): table with the mutations
        exons_coords_df (:class:`~pandas.DataFrame`): table with the genomic coordinates of the exons
        introns_coords_df (:class:`~pandas.DataFrame`): table with the genomic coordinates of the introns

    Returns:
        :obj:`tuple` of :class:`~pandas.DataFrame`. One DataFrame with the mutations that fall in any of the exons
        and the other with the ones falling in introns.

    """

    sub_mutations_df = mutations_df[['CHROMOSOME', 'POSITION', 'SAMPLE']].copy()
    sub_mutations_df['START'] = sub_mutations_df['POSITION'] - 1  # for pybedtools

    sub_mutations_df = sub_mutations_df[['CHROMOSOME', 'START', 'POSITION', 'SAMPLE']]
    sub_mutations_df['CHROMOSOME'] = sub_mutations_df['CHROMOSOME'].astype(str)

    sub_mutations_df.columns = ['chr', 'start', 'end', 'sample']
    exons_coords_df.columns = ['chr', 'start', 'end', 'ensembl']
    introns_coords_df.columns = ['chr', 'start', 'end', 'ensembl']

    sub_exons_coords_df = exons_coords_df[['chr', 'start', 'end', 'ensembl']]
    sub_introns_coords_df = introns_coords_df[['chr', 'start', 'end', 'ensembl']]

    exons_bed = pybedtools.BedTool.from_dataframe(sub_exons_coords_df)
    introns_bed = pybedtools.BedTool.from_dataframe(sub_introns_coords_df)
    sub_mutations_bed = pybedtools.BedTool.from_dataframe(sub_mutations_df)

    mutations_in_exons_bed = exons_bed.intersect(sub_mutations_bed)
    mutations_in_introns_bed = introns_bed.intersect(sub_mutations_bed)

    mutations_in_exons_symbol_df = pd.read_table(mutations_in_exons_bed.fn,
                                                 names=['chr', 'start', 'position', 'ensembl'])
    mutations_in_introns_symbol_df = pd.read_table(mutations_in_introns_bed.fn,
                                                   names=['chr', 'start', 'position', 'ensembl'])

    exons_bed.delete_temporary_history(ask=False)
    introns_bed.delete_temporary_history(ask=False)
    sub_mutations_bed.delete_temporary_history(ask=False)
    mutations_in_exons_bed.delete_temporary_history(ask=False)
    mutations_in_introns_bed.delete_temporary_history(ask=False)

    return mutations_in_exons_symbol_df, mutations_in_introns_symbol_df




def compute_syn_nonsyn_muts2(genes_list, mutations_in_exons_symbol_df, tb_consequence_type, mutations_df, CONSEQUENCE_RANK):
    """
    For each gene in the list, get the consequence type associated with each combination of reference and
    alternate triplets and merge it with the mutations.

    Args:
        genes_list (list): list of genes Ensembl identifiers
        mutations_in_exons_symbol_df (:class:`~pandas.DataFrame`): table with the exons genomic coordinates
        tb_consequence_type: tabix with the consequence types
        mutations_df :class:`~pandas.DataFrame`): table with the mutations
        CONSEQUENCE_RANK (:class:`~pandas.DataFrame`): table that maps each consequence type with its type
        (synonimous or non-synonimous)

    Returns:
        :class:`~pandas.DataFrame`. Table

    """

    my_position_results = list()

    for symbol_gene in genes_list:

        gene_mutations = mutations_in_exons_symbol_df[mutations_in_exons_symbol_df['ensembl'] == symbol_gene]
        gene_mutations_list = gene_mutations.values.tolist()

        # For every exonic mutation in every gene, know if it is synonymous or nonsynonymous

        if len(gene_mutations_list) > 0:

            for gene_mutation in gene_mutations_list:

                my_chr = gene_mutation[0][3:]
                my_start = int(gene_mutation[1])
                my_end = int(gene_mutation[2])

                res_exons = tb_consequence_type.querys("{}:{}-{}".format(my_chr, my_end, my_end))  # mutations are expected to have only 1 position so start and end are equal

                for my_result in res_exons:
                    my_result[5] = my_result[5].rstrip('\r')

                    signature = hg19(my_result[0], int(my_result[1])-1, 3)

                    tri_ref = signature[0] + my_result[2] + signature[-1]
                    tri_alt = signature[0] + my_result[3] + signature[-1]

                    my_result.append(tri_ref)
                    my_result.append(tri_alt)
                    my_result.append(symbol_gene)

                    my_position_results.append(my_result)

    my_position_results_df = pd.DataFrame(my_position_results)
    my_position_results_df.columns = ['CHROMOSOME', 'POSITION', 'REF', 'ALT', 'ENSEMBL_ONLY',
                                      'CONSEQUENCE', 'TRI_REF', 'TRI_ALT', 'ENSEMBL']

    my_position_results_df = my_position_results_df.drop('ENSEMBL_ONLY', 1)

    my_position_results_df = pd.merge(my_position_results_df, CONSEQUENCE_RANK, on='CONSEQUENCE')
    my_position_results_df['POSITION'] = my_position_results_df['POSITION'].astype(int)
    my_position_results_df['CHROMOSOME'] = my_position_results_df['CHROMOSOME'].astype(str)
    my_position_results_df.drop_duplicates(inplace=True)
    my_position_results_df['CHROMOSOME'] = 'chr' + my_position_results_df['CHROMOSOME'].astype(str)

    mutations_in_exons = pd.merge(my_position_results_df, mutations_df, on=['CHROMOSOME', 'POSITION', 'REF', 'ALT'])

    return mutations_in_exons


def compute_results_per_gene(sub_mutations_in_exons, my_gene, sub_exon, sub_intron, my_sample):
    """
    Extract the number of synonymous and non-synonymous mutations
    and their proportion and the exon and intron proportion.
    Additionally, the number of mutation in exons and introns is computed.

    Args:
        sub_mutations_in_exons (:class:`~pandas.DataFrame`): table with the exonic mutations and their type (syn vs. non-syn)
        my_gene (str): Ensembl identifier
        sub_exon (:class:`~pandas.DataFrame`): table with the mutations in exons
        sub_intron (:class:`~pandas.DataFrame`): table with the mutations in introns
        my_sample (str): sample identifier

    Returns:
        list. Gene identifier, number of mutations in exons and introns, number of mutations synonymous and
        non-synonymous mutations and sample identifier.

    """
    sub_gene_mutations = sub_mutations_in_exons[sub_mutations_in_exons['ENSEMBL'] == my_gene]

    n_syn_muts = len(sub_gene_mutations[sub_gene_mutations['TYPE_x'] == 'synonymous'])
    n_nonsyn_muts = len(sub_gene_mutations[sub_gene_mutations['TYPE_x'] == 'non_synonymous'])

    n_exon_muts = len(sub_exon[sub_exon['ensembl'] == my_gene])
    n_intron_muts = len(sub_intron[sub_intron['ensembl'] == my_gene])

    my_row = [my_gene, n_exon_muts, n_intron_muts, n_syn_muts,
              n_nonsyn_muts, my_sample]

    return my_row


def perform_analysis_per_group(n_groups, not_very_low_expression_df, very_low_expression_df):
    """Divide genes into groups depending on their expression level"""

    groups_size = int(len(not_very_low_expression_df.index.tolist()) / n_groups)

    expr_levels = dict()

    for i in range(n_groups):
        my_start = i * groups_size
        my_end = (i + 1) * groups_size

        if i == max(range(n_groups)):
            my_end = len(not_very_low_expression_df.index.tolist())

        sub_expression_df = not_very_low_expression_df.iloc[my_start:my_end]
        sub_expression_genes = sub_expression_df.index.tolist()

        expr_levels[i] = sub_expression_genes

    sub_expression_genes = very_low_expression_df.index.tolist()
    expr_levels['very_low'] = sub_expression_genes

    return expr_levels


def perform_analysis_per_group(n_groups, not_very_low_expression_df, very_low_expression_df):
    """divides genes into groups depending on their expression level"""

    groups_size = int(len(not_very_low_expression_df.index.tolist()) / n_groups)

    expr_levels = dict()

    for i in range(n_groups):
        my_start = i * groups_size
        my_end = (i + 1) * groups_size

        if i == max(range(n_groups)):
            my_end = len(not_very_low_expression_df.index.tolist())

        sub_expression_df = not_very_low_expression_df.iloc[my_start:my_end]
        sub_expression_genes = sub_expression_df.index.tolist()

        expr_levels[i] = sub_expression_genes

    sub_expression_genes = very_low_expression_df.index.tolist()
    expr_levels['very_low'] = sub_expression_genes

    return expr_levels
