README
======

This repository contains the code used for generating the data in the paper:
Reduced mutation rate in exons due to differential mismatch repair.

How to use
----------

The data was generated using a combination of Jupyter notebooks (using Python) and some external libraries and well as some bash and Perl scripts.

All notebooks are documented, so you can check which kind of data is needed to run them (and in which folder should it be) and what is the output that is generated. 

Before using
------------

First of all you need to get our repository:

.. code::

   git clone https://bitbucket.org/bbglab/intron_exon_mutrate.git

or 

download it from the `downloads page <https://bitbucket.org/bbglab/intron_exon_mutrate/downloads/>`_.

Directory structure
^^^^^^^^^^^^^^^^^^^

The **data** and **non_provided_data** folders contains external data used in our calculations.

In the **data** folder there is a ``REAME`` file that explains
what data is in each file and how we got it.

The **non_provided_data** folder is the one in which we have some data
that we are not sharing. See more information in the ``README`` file
located in that folder.

The **results** folder is used by the notebooks to write their output there.
In the corresponding notebooks you will find more information about 
the kind of data that each one generates.

The **figures** folder is used by some notebooks 
that generate plots to save the figures in there.

.. warning::

   You need to create it:

   .. code:: 

      mkdir figures


Installation
^^^^^^^^^^^^

For the Jupyter notebooks you need to have Python plus some other the packages.
If you are using conda, you can use the same environment as we used by 
generating the environment from the ``env.yml`` file:

.. code:: bash

   conda env create -f env.yml


Workflow
--------

The `sequences_filtering <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/sequences_filtering.ipynb>`_
notebook computes the sequences that are going to be used for each tumor type.
The output of this notebook is already provided in the **data** folder.


1. Execute the `signature_probability <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/signature_probability.ipynb>`_ notebook.

2. Run the main part of the analysis using the `gene_and_sample_analysis <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/gene_and_sample_analysis.ipynb>`_ .

3. The remaining analysis is done in the notebooks for each figure. 
   Those notebooks generate the data for the plots, save it and the do the plots.
   Thus, you can modify the plot parameters without recomputing.

   - `figure1 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figure1.ipynb>`_
   - `figure2 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figure2.ipynb>`_
   - `figure3 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figure3.ipynb>`_
   - `figure4 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figure4.ipynb>`_
   - `figure5 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figure5.ipynb>`_
   - `figureS1 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figureS1.ipynb>`_
   - `figureS2 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figureS2.ipynb>`_
   - `figureS3 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figureS3.ipynb>`_
   - `figureS4 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figureS4_S5.ipynb>`_
   - `figureS5 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figureS4_S5.ipynb>`_
   - `figureS6 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figureS6.ipynb>`_
   - `figureS7 <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/figureS7.ipynb>`_

4. The functions used to perform random permutation of mutations to estimate the expected exon and intron mutations for emprical p-value calculation (shown in Supp. Table 3) can be found in this `permutation_analysis_gene_tumortype <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/intron_exon_mutrate/raw/master/permutation_analysis_gene_tumortype.ipynb>`_ notebook.
